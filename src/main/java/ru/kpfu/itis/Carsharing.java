package ru.kpfu.itis;


import java.sql.*;

public class Carsharing {

    public static void main(String[] args) {
//        try (Session session = driver.session()) {
//
//            String orderResult = session.writeTransaction(new TransactionWork<String>() {
//
//                @Override
//                public String execute(Transaction transaction) {
//
//                    StatementResult result = transaction.run(
//                            "CREATE (a:USER {name:'vova', surname: 'Vovanov', phone: '88005553555'}),"
//                                    +
//                                    "(b:USER {name:'dima', surname: 'Dimin', phone: '88005553556'})," +
//
//
//                                    "(c:ORDER {date:'8.11.2018'})," +
//
//                                    "(d:TRACK {time:'3:45:00' ,latitude:'24.9094357340', longitude: '53.12312312'})," +
//
//                                    "(e:CAR {make:'Lada Priora', model:'Priora',number:'Т082УХ',type:'Седан',class:'B',price: '500',age:'2'})," +
//
//                                    "(c)-[:doneBy]->(a)," +
//                                    "(c)-[:contains]->(e)," +
//                                    "(e)-[:ownedBy]->(b)," +
//                                    "(c)-[:hasTrack]->(d)," +
//
//                                    " MATCH ( order:ORDER) -[:doneBy]-> (user:USER) " +
//                                    "WHERE order.date='8.11.2018' and user.name='vova'" +
//                                    "RETURN order.date"
//
//                    );
//
//                    return result.single().get( 0 ).asString();
//                }
//
//            });
//
//            System.out.println(orderResult);
//
//
//        }

        try (Connection con = DriverManager.getConnection("jdbc:neo4j:bolt://localhost/?user=neo4j,password=neo4j2,scheme=basic")) {

            Statement statement = con.createStatement();

            statement.executeQuery("CREATE (a:USER {name:'vova', surname: 'Vovanov', phone: '88005553555'}),"
                    +
                    "(b:USER {name:'dima', surname: 'Dimin', phone: '88005553556'})," +


                    "(c:ORDER {date:'8.11.2018'})," +

                    "(d:TRACK {time:'3:45:00' ,latitude:'24.9094357340', longitude: '53.12312312'})," +

                    "(e:CAR {make:'Lada Priora', model:'Priora',number:'Т082УХ',type:'Седан',class:'B',price: '500',age:'2'})," +

                    "(c)-[:doneBy]->(a)," +
                    "(c)-[:contains]->(e)," +
                    "(e)-[:ownedBy]->(b)," +
                    "(c)-[:hasTrack]->(d)");

            ResultSet rs = statement.executeQuery(
                    " MATCH (order:ORDER) -[:doneBy]-> (user:USER) " +
                            "WHERE order.date='8.11.2018' and user.name='vova'" +
                            "RETURN order.date");

            while (rs.next()) {
                System.out.println(rs.getString("order.date"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
